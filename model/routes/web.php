<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('user1/{id}', function($id) {
    echo App\cricket::find($id);
});
Route::get('query',function(Request $request){
       $data= App\cricket::chunk(3, function ($flights) {
           foreach ($flights as $flight) {
               echo "$flights"."<br />"."<br />";
           }
        });
});

Route::get('query2',function(Request $request){
    foreach(App\cricket::where('id', 1)->cursor() as $flight) {
        echo $flight;
    }
});
Route::get('query3{id}',function($id){
    return App\Flight::findOrFail($id);

});
Route::get('/home/{id}', function ($id) {

    return App\cricket::findOrFail($id);
});
Route::get('query4',function(){

    App\cricket::destroy( 3);
    echo "hello";
});
Route::get('query5',function(){

    $data =App\cricket::select('name');
    $users = $data->addSelect('run')->get();
    echo $users;
});
Route::resource('photo', 'PhotoController');

