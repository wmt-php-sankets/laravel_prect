<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'image'=>'App\Image',
    'video'=>'App\Video',
]);
class Post extends Model
{
    public function posttable()
    {
        return $this->morphTo();
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
