<?php

namespace App\Http\Controllers;
use App\flight;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;

class flightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "FLIGHT TABLE";
        $flight = flight::all();

        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Flight_No </th>";
        echo "<td>Flight_Name</td>";
        echo "<td>Total_Seat</td>";
        echo "<td>Start_Root</td>";
        echo "<td>End_Root</td>";

        echo "</tr>";

        foreach ($flight as $f) {
            echo "<tr>";
            echo "<td>$f->id</td>";
            echo "<td>$f->Flight_No</td>";
            echo "<td>$f->Flight_Name</td>";
            echo "<td>$f->Total_Seat</td>";
            echo "<td>$f->Start_Root</td>";
            echo "<td>$f->End_Root</td>";

            echo "</tr>";
        }
        echo "</table>";
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "<h3>CREATE</h3>";
        echo '<form action="'.route("home.store").'" method="POST">';
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="number" name="Flight_No" placeholder="enter the flight_no">';
        echo '<input type="text" name="Flight_Name" placeholder="enter the flight_name ">';
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   // dd(route('home.index'));
        echo "<h3>STORE</h3>";
        flight::create($request->all());
        return redirect()->route('home.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function demo(){
        echo "hello sanket";
    }
}
