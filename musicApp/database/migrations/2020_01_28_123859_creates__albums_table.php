<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Albums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('Artists_id');
            $table->string('Title');
            $table->integer('Year');
            $table->string('Cover_image');
            $table->date('Published_Date');
            $table->timestamps();

            //foreign key

            $table->foreign('Artists_id')
                ->references('id')
                ->on('Artists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Albums');
    }
}
