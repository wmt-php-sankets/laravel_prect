<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tracks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('Albums_id');
            $table->string('title');
            $table->string('File');
            $table->enum('track_type',['pop','classic','jazz','Rock','Disco']);
            $table->timestamps();

            $table->foreign('Albums_id')
                ->references('id')
                ->on('Albums')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tracks');
    }
}
