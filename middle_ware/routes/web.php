<?php

Route::middleware(['india'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('home', function () {
        return view('home');
    });
});
