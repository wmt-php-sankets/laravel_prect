<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function posts(){
        return $this->hasManyThrough(Post::class,Userl::class,'country_id','userl_id');
    }
}
