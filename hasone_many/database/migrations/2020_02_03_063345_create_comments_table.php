<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('body');
                $table->unsignedBigInteger('userl_id');
                $table->unsignedBigInteger('commentable_id');
                $table->string('commentable_type');
                $table->foreign('userl_id')->references('id')->on('userls');
                $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
