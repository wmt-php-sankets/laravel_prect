<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function dashboard(){
        return view('dashboard');
    }

    public function  postSignUp(Request $request){

        $email =$request['email'];

        $frist_name =$request['frist_name'];
        $password =bcrypt($request['password']);

        $user =new User();
        $user->email=$email;
        $user->frist_name=$frist_name;
        $user->password=$password;
        $user->save();

        return redirect()->route('dashboard');
        }
    public function  postSignIn(Request $request){
        if (Auth::attempt(['email'=>$request['email'],'password'=>  $request['password']])){
            return 'sasa';
        }
        return redirect()->back();
    }
}
