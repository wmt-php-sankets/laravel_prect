<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    $val1 ="is coming";
    return view('home',[
        'data1' =>$val1,
        ]);
});
Route::get('/about', function () {
    $x='helo';
    return view('about',compact('x'));
});
Route::get('/content1','helloController@index');
