<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
class videoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $x =video::all();
         echo "table";
         echo "<table border='1px'>";
                echo "<tr>";
                    echo "<th>Name</th>";
                    echo "<th>VIEW</th>";
                echo "</tr>";
    foreach ($x as $xa){
        echo "<tr>";
        echo "<th>$xa->name</th>";
        echo "<th>$xa->view</th>";
        echo "</tr>";
    }
        echo "</table>";

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      echo "table is created";
      echo '<form action="\video" method="POST">';
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="text" name="name"  placeholder="enter video name"  >';
        echo '<input type="number" name="view"  placeholder="enter views"  >';
        echo '<input type="submit" value="submit">';
        echo '</form>';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "<h3>STORE</h3>";
        Video::create($request->all());
        return redirect()->route('video.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        echo "<h3>SHOW</h3>";
        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>Name</th>";
        echo "<th>Views</th>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>$video->name</td>";
        echo "<td>$video->view</td>";
        echo "</tr>";
        echo "</table>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        echo "table is created";
        echo "<h3>EDIT: $video->name</h3>";
        echo "<form action='/video/$video->id' method='POST'>";
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="hidden" name="_method" value="PUT" >';
        echo "<input type='text' name='name'  placeholder='enter video name'  value='$video->name' >";
        echo "<input type='number' name='view'  placeholder='enter view'  value='$video->view' >";
        echo '<input type="submit" value="submit">';
        echo '</form>';
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Video $video)
    {
        $video->update($request->all());
        return redirect()->route('video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video  $video)
    {
        $video->delete();
        return redirect()->route('video.index');
    }
}
