<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = [
        'title', 'pages', 'langauge','cover_image','isbn','description','author_id'
    ];
    public function authors()
    {
        return $this->belongsTo('App\Book');
    }
}
