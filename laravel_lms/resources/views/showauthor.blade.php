@extends('layouts.app')

@section('content')

<table class="table table-striped table-hover table-dark">
    <thead>
    <tr>
        <th scope="col">FullName</th>
        <th scope="col">Dob</th>
        <th scope="col">Gender</th>
        <th scope="col">Address</th>
        <th scope="col">Mobile_no</th>
        <th scope="col">Description</th>
        <th scope="col">Status</th>
        <th scope="col">Created At</th>
        <th scope="col">Updated At</th>
        <th scope="col"><Activity></Activity></th>
    </tr>
    </thead>
        <tbody>
            <tr>
              <th scope="row">{{$author->fullname}}</th>
                <td>{{$author->dob}}</td>
                <td>{{$author->gender}}</td>
                <td>{{$author->address_ }}</td>
                <td>{{$author->mobile_no}}</td>
                <td>{{$author->description_}}</td>
                <td>{{$author->status}}</td>
                <td>{{$author->created_at}}</td>
                <td>{{$author->updated_at}}</td>
            </tr>

        </tbody>
</table>
@endsection
