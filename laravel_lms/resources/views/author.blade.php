@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <button class="btn btn-primary mx-auto"><a href="{{ route('author.create') }}" class="text-white">Fill Author Data</a></button>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-dark">
                            <thead>
                            <tr>
                                <th scope="col">FullName</th>
                                <th scope="col">Dob</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                                <th scope="col">Mobile_no</th>
                                <th scope="col">Description</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                                <th scope="col"><Activity></Activity></th>
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($author as $as)
                            <tr>
                                <th scope="row">{{$as->fullname}}</th>
                                <td>{{$as->dob}}</td>
                                <td>{{$as->gender}}</td>
                                <td>{{$as->address_ }}</td>
                                <td>{{$as->mobile_no}}</td>
                                <td>{{$as->description_}}</td>
                                <td>{{$as->status}}</td>
                                <td>{{$as->created_at}}</td>
                                <td>{{$as->updated_at}}</td>
                                <td><a class="btn btn-primary" href="{{ route('author.edit',$as->id) }}">Edit</a></td>
                                <td>
                                <form action="{{ route('author.destroy',$as->id) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                                 </td>
                                    <td><button class="btn btn-info "><a href="{{route('author.show',$as->id)}}">Show</a></button></td>
{{--                                <td><button class="btn btn-info "><a href="{{route('author.updateauthorStatus',$as->id)}}">ACTIVE</a></td>--}}
                            </tr>
                        @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
