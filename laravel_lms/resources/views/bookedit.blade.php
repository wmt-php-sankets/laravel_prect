@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ route('book.update',$book->id) }}" method="POST" id="myform" name="form">

            @method('PUT')
            @csrf

            <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" name="title" id="title" class="form-control" aria-describedby="emailHelp" value={{$book->title}} placeholder="Enter Book Title">
                <span class="text-danger">  @error('title')
                    {{$message}}
                    @enderror
                </span>
            </div>



            <div class="form-group">
                <label for="exampleInputEmail1">Pages</label>
                <input type="number" name="pages" id="pages" class="form-control" id="lastname" aria-describedby="emailHelp"  value={{$book->pages}} placeholder="Enter Pages">
                <span class="text-danger">  @error('pages')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">langauge</label>
                <input type="text" class="form-control" name="langauge" value={{$book->langauge}} id="langauge" placeholder="Enter Book Langauge">
               <span class="text-danger">
                @error('langauge')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="">Book Author</label>
                <select class="" name="author" id="author">
                </select>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Cover Image</label>
                <input type="file" class="form-control" id="image" name="image" value={{$book->cover_image}} placeholder="Enter Cover Image">
                <span class="text-danger">  @error('image')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">ISBN Number</label>
                <input type="number" class="form-control" name="isbn" id="isbn" value={{$book->isbn}} placeholder="Isbn Number">
                <span class="text-danger">  @error('isbn')
                    {{$message}}
                    @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Description</label><br>
                <textarea rows="4" cols="50" id="desc" name="description"  placeholder="Description">
                    {{$book->description}}
                </textarea>
            </div>

            <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

        </form>
    </div>
@endsection
