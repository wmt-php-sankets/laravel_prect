@extends('layouts.app')

@section('content')
    <div class="container">



        <form action="{{ route('book.store') }}" method="POST" id="myform" name="form">
            @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" name="title" id="title" class="form-control" aria-describedby="emailHelp" placeholder="Enter Book Title">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Pages</label>
            <input type="number" name="pages" id="pages" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Pages">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">langauge</label>
            <input type="text" class="form-control" name="langauge" id="langauge" placeholder="Enter Book Langauge">
        </div>



            <div class="dropdown show">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   Book Author
                </a>
                        @foreach($author as $au)
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                    <a class="dropdown-item" href="#" name="author_id"  value="{{$au->fullname}}" >{{$au->fullname}}</a>
                </div>
                       @endforeach
            </div>





        <div class="form-group">
            <label for="exampleInputPassword1">Cover Image</label>
            <input type="file" class="form-control" id="image" name="cover_image" placeholder="Enter Cover Image">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">ISBN Number</label>
            <input type="text" class="form-control" name="isbn" id="isbn" placeholder="Isbn Number">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Description</label><br>
            <textarea rows="4" cols="50" id="desc" name="description" placeholder="Description">
    </textarea>
        </div>

        <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

    </form>
    </div>
@endsection






















