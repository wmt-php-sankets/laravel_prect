<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    public function phone(){
        return $this->hasMany(phone::class,'customer_id','id');
    }
    protected $fillable = [
        'city'
    ];

}
