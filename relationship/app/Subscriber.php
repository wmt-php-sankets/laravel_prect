<?php

namespace App;
use App\Role;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function role(){
        return $this->belongsToMany(role::class,'role_subscriber','role_id','subscriber_id')->wherePivotIn('id', [3]);

    }
}
