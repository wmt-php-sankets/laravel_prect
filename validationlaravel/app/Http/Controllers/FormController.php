<?php

namespace App\Http\Controllers;

use App\Form;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $x =Form::all();
        echo "table";
        echo "<table border='1px'>";
        echo "<tr>";
        echo "<th>Name</th>";
        echo "<th>data</th>";
        echo "<th>password</th>";
        echo "<th>address</th>";
        echo "<th>country</th>";


        echo "</tr>";
        foreach ($x as $xa){
            echo "<tr>";
            echo "<th>$xa->name</th>";
            echo "<th>$xa->date</th>";
            echo "<th>$xa->password</th>";
            echo "<th>$xa->address</th>";
            echo "<th>$xa->country</th>";

            echo "</tr>";
        }
        echo "</table>";
        return view('showdata');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|max:10|min:4',
            'date' => 'required',
            'password' => 'required|not_regex:/^.+$/i|numeric|',
            'address' => 'nullable',
            'country' => 'required',

        ],[
            "name.required"=>"name is require",
            "date.required"=>'date is require'
        ]);
        Form::create($request->all());
        return redirect()->route('form.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }
}
