@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="/form" method="POST">
                @csrf
                <div class="form-group">
                    <label for="city">name</label>
                    <input type="text" value="{{old('name ')}}" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter name" name="name">
                    @error('name')
                    <div class="alert alert-danger " >{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">date</label>
                    <input type="date"  value="{{old('date')}}" class="form-control " name="date" id="exampleInputPassword1" placeholder="Password">
                    @error('date')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                    @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">address</label>
                    <input type="text" class="form-control" name="address" id="exampleInputPassword1" placeholder="Enter Address">
                    @error('address')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Example select</label>
                    <select class="form-control" name="country" id="exampleFormControlSelect1">
                        <option value="india">india</option>
                        <option value="usa">usa</option>
                        <option value="pakistan">pakistan</option>
                        <option value="africa">africa</option>
                        <option value="canada">canada</option>
                    </select>
                    @error('country')
                    {{$message}}
                    @enderror
                </div>


                <button type="submit" class="btn float-right btn-primary">Submit</button>
            </form>
        </div>
@endsection
