<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('app1', function () {
    //    $a=::get()->toJson();
    //    return $a->toJson();
//
//    $user =App\Student::all();
//    $a= $user->toJson(JSON_PRETTY_PRINT);

    //return $user->toJson();
//
        return view('app');
});

//Route::view('/about','about');
//Route::view('/service','service');
Route::get('/app', 'helloController@index');
Route::get('/about', 'helloController@about');
Route::get('/service', 'helloController@service');

Route::get('blade', function () {
    return view('child');
});
Route::get('greeting', function () {
    return view('content', ['name' => 'Samantha']);
});
