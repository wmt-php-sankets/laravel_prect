@extends('app')
@section('title','Services')
@section('content')
    @includeFirst(['custom.demo', 'demo1'], ['some' => 'data'])
    @php('some')
    @endphp
    @include('demo')
    @include('demo', ['some' => 'data'])
{{--    @includeWhen($boolean, 'demo', ['some' => 'data'])--}}
{{--    @includeIf('demos', ['some' => 'data is not found'])--}}
<h2>i am service</h2>
    <ul>
        @forelse($service as $a)
            <li> {{$a}}<li>
            @empty
            <li> no service </li>
            @endforelse

                @foreach($service as $b)
                <li>{{$loop->parent}}->{{$b}} </li>
                @endforeach
            @if (count($service) === 1)
                I have one record!
            @elseif (count($service) > 1)
                I have multiple records!
            @else
                I don't have any records!
            @endif

            @section('sidebar')


                <p>This is appended to the master sidebar.</p>
            @endsection

            @section('sidebar')
                @parent
                <p>This is my body content.</p>
            @endsection
    </ul>
@endsection
