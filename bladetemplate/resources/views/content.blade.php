Hello, {{ $name }}.
<h1>Laravel</h1>

@verbatim
    <div class="container">
        Hello, @{{ var name }}.
    </div>
@endverbatim
