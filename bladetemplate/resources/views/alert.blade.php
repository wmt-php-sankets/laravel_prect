<p class="alert alert-{{$type}}">
    <strong>{{$title}}</strong>
    {{$slot}}
</p>
